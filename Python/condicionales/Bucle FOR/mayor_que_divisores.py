#Escriba un programa que pida un número entero mayor que cero y que escriba sus divisores.

from unicodedata import name


print("NUmero mayor y divisores ")
def main():
    numero=int(input("Escriba un numero mayor que 0"))

    if(numero<=0):
        print("Por favor ingrese un numero mayor que 0")
    else:
        print(f"Los numeros divisores de {numero} son ", end="")
        for i in range (1, numero+1):
            if (numero % i ==0):
                print(f"El {i} es divisor de {numero}")

if __name__ == "__main__":
    main()      