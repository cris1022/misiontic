# . Una empresa produce tanques en acero inoxidable y posee un lote de X
# piezas producidas. Diseñar, elaborar un programa en Python que pida 
# ingresar al operario de la empresa por teclado la cantidad de tanques a 
# procesar y luego ingrese la longitud de cada tanque en acero; sabiendo que 
# el tanque cuya longitud esté comprendida en el rango de 2.20 y 2.45 son 
# aptas. Imprimir por pantalla la cantidad de piezas aptas que hay en el lote