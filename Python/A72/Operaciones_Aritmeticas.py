#suma +

dato1=int(15)
dato2=float(2.9854)

suma=dato1+dato2
print(suma)
print(type(suma))
# redondear un resultado
print("%.2f"%(suma))
# %d->numeros enteros 
# %f->numero flotantes

#imprimir con cadena de texto 
print("la sumatoria de %d + %f = %2.f"%(dato1,dato2,suma))
#imprimir con cadena de texto limitando decimales 
print("Respuesta {} + {} es igual a = {:.3f}" .format(dato1,dato2,suma))

#Otra fomra de impresion 
print(f"La suma de {dato1} y {dato2} es igual a {suma}")

#resta 
resta=dato2-dato1
#imprimir con cadena de texto 
print("la resta de %f - %d = %2.f"%(dato2,dato1,resta))
#imprimir con cadena de texto limitando decimales 
print("Respuesta {} - {} es igual a = {:.3f}" .format(dato2,dato1,resta))

#Otra fomra de impresion 
print(f"La resta de {dato2} y {dato1} es igual a {resta}")

#Multiplicacion 
multiplicacion=dato2*dato1
#imprimir con cadena de texto 
print("la multiplicacion de %f * %d = %2.f"%(dato2,dato1,multiplicacion))
#imprimir con cadena de texto limitando decimales 
print("Respuesta {} * {} es igual a = {:.3f}" .format(dato2,dato1,multiplicacion))

#Otra fomra de impresion 
print(f"La multiplicacion de {dato2} y {dato1} es igual a {multiplicacion}")

#Division
Division=dato2/dato1
#imprimir con cadena de texto 
print("la Division de %f / %d = %2.f"%(dato2,dato1,Division))
#imprimir con cadena de texto limitando decimales 
print("Respuesta {} / {} es igual a = {:.3f}" .format(dato2,dato1,Division))

#Otra fomra de impresion 
print(f"La Division de {dato2} y {dato1} es igual a {Division}")

##division // es para division entrera 


#Potenciacion

potencia=dato1**dato2

print(f"el numero {dato1} elevado a {dato2} es igual a {potencia}")


#modulo o residuoi de la division

modulo=dato2%dato1

print(modulo)


