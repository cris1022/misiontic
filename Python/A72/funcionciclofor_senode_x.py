# Definir una función para calcular el seno de x empleando la Serie Taylor, los 
# datos de entrada de la función son el ángulo el cual se debe convertir a 
# radianes y la cantidad de veces a iterar la función

from math import pi,factorial,sin

def funSenoSerieTaylor(angulo,cantidadTerminos):
    anguloRadian=(angulo/180)*3.1415265
    senx=0.0
    
    factor = factorial
    rta=[]#la lista vacia
    for i in range(0,cantidadTerminos,1):
        #Funcion Seno
        exponente=(2*i+1)
        numerador=anguloRadian**exponente
        denominador=factor(exponente)
        
        senx= senx+(-1)**i* (numerador/denominador)
        resultado="{} 'sen' (x) {}".format(i+1,senx)
        rta.append(resultado)#añado el valor a la lista
        
    return rta#(i+1, 'senx', senx)    
    
    

angulo=37

cantidadTerminos= int(input("Ingrese la Cantidad de terminos"))
print(funSenoSerieTaylor(angulo,cantidadTerminos))

print(funSenoSerieTaylor(53,20))
print(funSenoSerieTaylor(90,20))
print(sin(angulo/180)*3.1415265), sin(53/100)*3.1415265,sin

 