#ejercicio 10
# #Emplee un ciclo while en python para crear menú con el cual cada opción 
# del menú enumerados del 1 al 8, las opciones del 1 a 7 cada una de ellas 
# será para cada operación matemática vista en la semana 1 (suma, resta,
# multiplicación, división, potencia, modulo o residuo de la división, raiz) la 
# opción 8 es para salir del programa. Evalúe el número ingresado el cual debe
# ser entero int() si escribe un valor diferente NO debe terminar la ejecución, 
# solamente termina la ejecución cuando se hace cualquiera de las 
# operaciones o digitó la opción de salir

# calculadora 
# Variable de control bandera

salir=False
opcion=int(0)

#\t=>es el tabulador 

#=>salto de linea

while not salir:
    
    print("""*-*-*-*-*-*-*-*-*-*Menu de operaciones*-*-*-*-*-*-*-*--*-*-*
          
    \t 1. Si desea sumar + Ingrese la opcion 1      
    \t 2. Si desea restar - Ingrese la opcion 2  
    \t 3. Si desea multiplicar * Ingrese la opcion 3     
    \t 4. Si desea dividir / Ingrese la opcion 4  
    \t 5. Si desea potenciar ** Ingrese la opcion 5      
    \t 6. Si desea el modulo  '%' Ingrese la opcion 6  
    \t 7. Si desea raiz n Ingrese la opcion 7      
    \t 8. Si desea salir Ingrese la opcion 8          
    \t Ingrese UNA opcion de las anteriores \n""")
    # try manejo de exepciones o manejo de errores 
    num=int(0)
    try:
        num =int(input())
    except Exception as ex:
        print("Error no es una opcion valida", type(ex))
        
    opcion=num   
    if opcion==1:
        print("suma")
        dato1=float(input("ingrese un el primer numero a sumar "))
        dato2=float(input("ingrese un el segundo numero a sumar "))
        suma=float(dato1+dato2)
        print(f"la suma de {dato1} + {dato2} es = {suma}")
        
    elif opcion==2:
        print("resta")
        dato1=float(input("ingrese un el primer numero  "))
        dato2=float(input("ingrese un el segundo numero a restar "))
        resta=float(dato1-dato2)
        print(f"la resta de {dato1} - {dato2} es = {resta}")
    elif opcion==3:
        print("multiplicar") 
        dato1=float(input("ingrese un el primer numero a multiplicar  "))
        dato2=float(input("ingrese un el segundo numero a multiplicar "))
        multiplicar=float(dato1*dato2)
        print(f"la multiplicacion de {dato1} * {dato2} es = {multiplicar}")
    elif opcion==4:
        print("dividir")
        dato1=float(input("ingrese un el primer numero a dividendo  "))
        dato2=float(input("ingrese un el segundo numero a divisor "))
        try:
            divi=float(dato1/dato2)
            print(f"la division de {dato1} / {dato2} es = {divi}")
        except ZeroDivisionError as error:
            print("Error de division entre 0 no puede hacer", type(error))    
    elif opcion==5:
        print("potencia")
        dato1=float(input("ingrese un el primer numero a potenciar  "))
        dato2=float(input("ingrese la potencia "))
        pote=float(dato1**dato2)
        print(f"la potecnia de {dato1} elevado a {dato2} es = {pote}")
    elif opcion==6:
        print("modulo") 
        dato1=float(input("ingrese un el primer numero a dividendo  "))
        dato2=float(input("ingrese el numero divisor "))
        modu=float(dato1%dato2)
        print(f"el modulo de {dato1} en {dato2} es = {modu}")
    elif opcion==7:
        print("raiz")
        dato1=float(input("ingrese el numero a sacar raiz   "))
        dato2=float(input("ingrese el numero raiz "))
        raiz=float(dato1**(1/dato2))
        print(f"la raizz {dato2} de  {dato1} es = {raiz}")
    elif opcion==8:
        print("Opcion de salir, Adios .......") 
        salir=True  
    else: 
        
        print("debe ingresar un valor de 1 a 8")                 
                   
        
   
    
print("Termina la ejecución")        
            