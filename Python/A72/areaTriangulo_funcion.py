# Se sabe que el área de un triángulo está asociada a los lados del triángulo 
# de la siguiente manera: A = sqrt(s*(s - a)*(s - b)*(s - c))
# La cual es la fórmula de Herón, donde a, b y c son los lados del triángulo y s es 
# el semiperímetro dado por: s = (a+b+c)/2
# Escriba una función llamada area_triangulo que usando estas dos funciones 
# determine el área de un triángulo a partir de sus lados. La función debe recibir 
# como parámetro de entrada los lados del triángulo y retornar el valor del área
from math import sqrt as raiz

def area_triangulo(a,b,c):
    semi=(a+b+c)/2
    if a+b>=c:
        Area=raiz(semi*(semi-a)*(semi-b)*(semi-c))
        Area=round(Area,4)
    else:
        Area="No se puede crear el triangulo con esos dato la suma de a y b debe ser mayor a c"
    return Area
print (area_triangulo (1,1,1))  
print(area_triangulo(4,6,5)) 
print(area_triangulo(2,2,5))       