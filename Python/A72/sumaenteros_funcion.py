#  Crear una función únicamente para sumar números enteros, use el bloque 
# try except y la declaración raise que permite al programador forzar a que 
# ocurra una excepción específica. Además, emplee a isinstance() función 
# devuelve la suma y True si el objeto especificado es del tipo especificado, 
# en caso contrario un mensaje “Valores deben ser enteros” y False.

def sumar(x,y):
    if isinstance(x,int) and isinstance(y,int):  #->isinstanceevalua que la variable pertezca a un tipo de dato 
        return x+y
    else: raise TypeError("Los Argumentos deben ser enteros ")
    
try:
    print(sumar(15,45))
    print(sumar(1151545,989521521))
    print(sumar(15,45.56))
    print(sumar("15","45"))
except TypeError as e:
    print(e)    


    