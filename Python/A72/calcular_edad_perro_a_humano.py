#  Un veterinario desea optimizar el cálculo para conocer la edad de los perros 
# que atiende en el consultorio, para ello se emplea la siguiente ecuación:
# ��������� = 16 ∗ ln(�) + 31
from math import log1p

def edad_perr_a_humano(edad=float):
    resultado=""
    try:
        if edad>0:
            edadperro=16*log1p(edad-1)+31  # el menos 1 es para caLCULAR EL LOGARITMO NATURTL DEBEMS RESTARLE 1
            edadperro=round(edadperro,3)
            resultado+="la edad {} años caninops son edad humana {}"\
                .format(edadperro)
        else:
            resultado="La edad Ingresada en menos o igual a 0"    
                
            
    except ValueError as error:
        resultado ="Hay un Erro"+error
    except Exception:
        resultado ="Tenemos Errors"    
        
        
    return resultado    

print(edad_perr_a_humano(1))
print(edad_perr_a_humano(2))    
print(edad_perr_a_humano(4))    
print(edad_perr_a_humano(6))    
print(edad_perr_a_humano(0))    
print(edad_perr_a_humano("hola"))            
