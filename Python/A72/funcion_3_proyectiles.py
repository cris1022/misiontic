# En el lanzamiento de proyectiles existen dos fórmulas que permiten obtener 
# dos parámetros importantes, uno es la altura máxima alcanzada por el 
# proyectil (y_max) y el otro es la distancia máxima recorrida (x_max). Estas 
# expresiones son: 
# �!"# = �$
# % ∗ sin(2 ∗ �)
# �
# �!"# = �$
# % ∗ sin%(�)
# 2 ∗ �
# Donde g es la gravedad, g = 9.86 m/s2, Vo es la velocidad inicial a la que es 
# lanzado el proyectil y theta el ángulo de inclinación al cual es lanzado el proyectil. 
# Cree una función que se llame proyectil y que retorne, para un proyectil que el 
# lanzado a un ángulo theta (ángulo en grados) a una velocidad inicial Vo (en 
# metros/segundos), la altura máxima Ymax (en metros) y el máximo 
# recorrido Xmax (en metros). La función recibe como parámetros de entrada, y en 
# el siguiente orden, el ángulo theta y la velocidad inicial Vo.
# Tip: Para el uso de la función seno (sin) se requiere importarla primero desde el 
# módulo math. Adicionalmente, debido a que las funciones trigonométricas reciben 
# los ángulos en radianes hay que tener en cuenta la conversión de grados a 
# radianes que es: angulo_radianes = angulo_grados*PI/180, con el número PI 
# estando definido en el módulo math
import math as matematica

def proyectil(angulo,Vo):
    gravedad=9.86
    anguloRadian=(angulo*matematica.pi)/180
    
    distmax= int((Vo**2)*matematica.sin(2*anguloRadian))/gravedad
    altmax= int((Vo**2)*matematica.sin(anguloRadian)**2)/(2*gravedad)
    
    return distmax,altmax
print(proyectil(45,10))
print(proyectil(80,45))